import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { ProfesionalExperienceComponent } from './components/profesional-experience/profesional-experience.component';
import { AboutComponent } from './components/about/about.component';
import { EducationComponent } from './components/education/education.component';
import { SkillsComponent } from './components/skills/skills.component';
import { CertificationsComponent } from './components/certifications/certifications.component';
import { ContactComponent } from './components/contact/contact.component';
import { GitReposComponent } from './components/git-repos/git-repos.component';
import { PublicPortfolioComponent } from './components/public-portfolio/public-portfolio.component';
import { FooterComponent } from './components/footer/footer.component';
import { I18nPipe } from './pipes/i18n.pipe';
import { LanguageMenuComponent } from './components/language-menu/language-menu.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    ProfesionalExperienceComponent,
    AboutComponent,
    EducationComponent,
    SkillsComponent,
    CertificationsComponent,
    ContactComponent,
    GitReposComponent,
    PublicPortfolioComponent,
    FooterComponent,
    I18nPipe,
    LanguageMenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
