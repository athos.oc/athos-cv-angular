import es from './es.json'
import en from './en.json'

export class I18n{

    private static selectedLanguage=null

    public static setLanguage(language='en'){
        switch (language) {
            case 'es':
                I18n.selectedLanguage = es
                break;
            case 'en':
                I18n.selectedLanguage = en
                break;
            default:
                I18n.selectedLanguage = en
                break;
        }
    }

    public static T(key){
        if(I18n.selectedLanguage === null){
            I18n.setLanguage('en')
        }
        return I18n.selectedLanguage[key]
    }
}