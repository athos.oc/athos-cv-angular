import { Pipe, PipeTransform } from '@angular/core';
import { I18n } from '../i18n/i18n'

@Pipe({
  name: 'i18n'
})
export class I18nPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    return I18n.T(value)
  }

}
