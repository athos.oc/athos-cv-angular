import { Component, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-language-menu',
  templateUrl: './language-menu.component.html',
  styleUrls: ['./language-menu.component.scss']
})
export class LanguageMenuComponent {
  @Input() language='en'
  @Output() changeLanguageClick = new EventEmitter()

  constructor() { }

  languageToggle(language){
    this.language = language
    this.changeLanguageClick.emit(language)
  }
}
