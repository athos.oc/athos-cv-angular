import { I18n } from './../../i18n/i18n';
import { Component } from '@angular/core';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  showFlag=true
  currentLanguage='en'

  constructor() {
    const languagePreference = localStorage.getItem('languagePreference')
    console.log('languagePreference',languagePreference)
    if(languagePreference!==null){
      this.changeLanguageClick(languagePreference)
    }
  }

  changeLanguageClick(currentLanguage){
    localStorage.setItem('languagePreference', currentLanguage)
    this.currentLanguage = currentLanguage
    I18n.setLanguage(currentLanguage)
    this.showFlag=false
    setTimeout(() => {
      this.showFlag=true
    }, 10);
  }
}
